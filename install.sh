#! /usr/bin/env zsh

# systemtheme directories
systemtheme_dir=${0:a:h}
systemtheme_config_dir=$HOME/.config/systemtheme
systemtheme_func_dir=/usr/local/bin

# config files
kitty_config=$HOME/.config/kitty/kitty.conf
dunst_config=$HOME/.config/dunst/dunstrc

# replace user config files in systemtheme functions
sed -i "s;source.*;source\ $systemtheme_config_dir/themes.sh;g" $systemtheme_dir/systemtheme.sh
sed -i "s;^kitty_config=.*;kitty_config=$kitty_config;g" $systemtheme_dir/STKitty.sh
sed -i "s;^dunst_config=.*;dunst_config=$dunst_config;g" $systemtheme_dir/STDunst.sh

# add themes file to config directory
[ ! -d $systemtheme_config_dir ] && mkdir -p $systemtheme_config_dir
[ ! -f $systemtheme_config_dir/themes.sh ] && cp $systemtheme_dir/themes.sh $systemtheme_config_dir

# add launching scripts to your shell utils
[ ! -d $systemtheme_func_dir ] && sudo mkdir -p $systemtheme_func_dir
sudo cp $systemtheme_dir/systemtheme.sh $systemtheme_func_dir/systemtheme
sudo cp $systemtheme_dir/STKitty.sh $systemtheme_func_dir/STKitty
sudo cp $systemtheme_dir/STDunst.sh $systemtheme_func_dir/STDunst
