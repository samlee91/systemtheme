#! /usr/bin/env zsh

#             Backgnd     Red      Green    Yellow     Blue     Magenta    Cyan     Foregnd    White   Backgnd2    Red2     Green2    Yellow2    Blue2   Magenta2    Cyan2    Foregnd2
    onedark=("#282c34" "#e06c75" "#98c379" "#e5c07b" "#61afef" "#c678dd" "#56b6c2" "#abb2bf" "#dfdfdf" "#3d4148" "#be5046" "#98c379" "#d19a66" "#61afef" "#c678dd" "#56b6c2" "#596276")
    dracula=("#282a36" "#ff5555" "#50fa7b" "#f1fa8c" "#bd93f9" "#ff79c6" "#8be9fd" "#f8f8f2" "#f8f8f2" "#44475a" "#ff5555" "#50fa7b" "#f1fa8c" "#bd93f9" "#ff79c6" "#8be9fd" "#6272a4")
    gruvbox=("#282828" "#FB4934" "#B8BB26" "#FABD2F" "#83a598" "#d3869b" "#8ec07c" "#ebdbb2" "#ebdbb2" "#928374" "#cc241d" "#98971a" "#d79921" "#458588" "#b16286" "#689d6a" "#a89984")
color_names=("BACKGROUND" "RED" "GREEN" "YELLOW" "BLUE" "MAGENTA" "CYAN" "FOREGROUND" "WHITE" "BACKGROUND_ALT" "RED_ALT" "GREEN_ALT" "YELLOW_ALT" "BLUE_ALT" "MAGENTA_ALT" "CYAN_ALT" "FOREGROUND_ALT")

