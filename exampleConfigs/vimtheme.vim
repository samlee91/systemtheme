
" http://vimdoc.sourceforge.net/htmldoc/syntax.html

set background=dark
highlight clear

if exists("syntax_on")
   syntax reset
endif

set t_Co=256



function SimpleColorize(highlight_group, color)
   exec "highlight " . a:highlight_group . " guifg=" . a:color
   exec "highlight " . a:highlight_group . " guibg='NONE' cterm='NONE'"
endfunc


function Colorize(highlight_group, foreground, background, style)
   exec "highlight " . a:highlight_group . " guifg=" . a:foreground
   exec "highlight " . a:highlight_group . " guibg=" . a:background
   exec "highlight " . a:highlight_group . " cterm=" . a:style
endfunc


" Main Syntax Groups
call SimpleColorize('Character',      $GREEN)
call SimpleColorize('Comment',        $FOREGROUND_ALT)
call SimpleColorize('Constant',       $CYAN)
call SimpleColorize('Conditional',    $MAGENTA)
call SimpleColorize('String',         $GREEN)
call SimpleColorize('Statement',      $MAGENTA)
call SimpleColorize('Function',       $BLUE)
call SimpleColorize('Special',        $BLUE)
call SimpleColorize('Repeat',         $MAGENTA)
call SimpleColorize('Operator',       $MAGENTA)
call SimpleColorize('Keyword',        $MAGENTA)
call SimpleColorize('Label',          $MAGENTA)
call SimpleColorize('Exception',      $MAGENTA)
call SimpleColorize('Include',        $MAGENTA)
call SimpleColorize('Define',         $MAGENTA)
call SimpleColorize('Precondit',      $MAGENTA)
call SimpleColorize('StorageClass',   $MAGENTA)
call SimpleColorize('Number',         $YELLOW_ALT)
call SimpleColorize('Boolean',        $YELLOW_ALT)
call SimpleColorize('Float',          $YELLOW_ALT)
call SimpleColorize('SpecialComment', $YELLOW_ALT)
call SimpleColorize('Preproc',        $YELLOW)
call SimpleColorize('Type',           $YELLOW)
call SimpleColorize('Tag',            $YELLOW)
call SimpleColorize('Identifier',     $CYAN)
call SimpleColorize('Macro',          $CYAN)
call SimpleColorize('Structure',      $CYAN)
call SimpleColorize('TypeDef',        $CYAN)
call SimpleColorize('SpecialChar',    $CYAN)
call SimpleColorize('Underlined',     $CYAN)
call SimpleColorize('Debug',          $RED_ALT)
call SimpleColorize('Error',          $RED)
call SimpleColorize('Ignore',         $BACKGROUND_ALT)
call SimpleColorize('Delimiter',      $FOREGROUND)


" Text Groups
call Colorize('MatchParen',    $BLUE, 'NONE', 'bold')
call Colorize('SpellBad',      $RED,  'NONE', 'underline')


" Menu Groupe
call Colorize('Pmenu',        $WHITE, $FOREGROUND_ALT, 'NONE')
call Colorize('PmenuSel',     $BACKGROUND, $BLUE, 'NONE')
call Colorize('PmenuSbar',    $FOREGROUND_ALT, $FOREGROUND_ALT, 'NONE')
call Colorize('PmenuThumb',   $FOREGROUND_ALT, $FOREGROUND_ALT, 'NONE')


" Editor Groups
call Colorize('LineNr',        $FOREGROUND_ALT, 'NONE', 'NONE')
call Colorize('CursorLine',    'NONE', $BACKGROUND_ALT, 'NONE')
call Colorize('CursorLineNr',  $WHITE, $BACKGROUND_ALT, 'bold')
call Colorize('Folded',        $FOREGROUND_ALT, 'NONE', 'italic')
call Colorize('FoldColumn',    $BACKGROUND, 'NONE', 'NONE')
call Colorize('EndOfBuffer',   $FOREGROUND_ALT, 'NONE', 'NONE')
call Colorize('TabLineSel',    $BACKGROUND, $FOREGROUND, 'bold')
call Colorize('Tabline',       $FOREGROUND_ALT, 'NONE', 'italic')
call Colorize('TabLineFill',   $BACKGROUND, 'NONE', 'NONE')
call Colorize('StatusLine',    $BACKGROUND, 'NONE', 'NONE')
call Colorize('StatusLineNC',  $BACKGROUND, 'NONE', 'bold')
call Colorize('VertSplit',     $FOREGROUND_ALT, 'NONE', 'NONE')
call Colorize('Visual',        'NONE', $BACKGROUND, 'inverse')


" Bold Colors
call Colorize('BoldRed',     $RED, 'NONE', 'bold')
call Colorize('BoldYellow',  $YELLOW, 'NONE', 'bold')
call Colorize('BoldGreen',   $GREEN, 'NONE', 'bold')
call Colorize('BoldBlue',    $BLUE, 'NONE', 'bold')
call Colorize('BoldCyan',    $CYAN, 'NONE', 'bold')
call Colorize('BoldMagenta', $MAGENTA, 'NONE', 'bold')
call Colorize('BoldWhite',   $WHITE, 'NONE', 'bold')



" Set Chars for Columns and Folds
set fillchars=vert:┃,fold:━,eob:\ 


" overwrite colors/styles for tabline and statusline
let g:themed_plugins = ["statusline", "tabline"]

for themed_file in g:themed_plugins
   exec "source ~/.vim/plugin/" . themed_file . ".vim"
endfor

