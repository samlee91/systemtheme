# Systemtheme
![](screenshot.png)

`systemtheme` gives the user the ability to theme your window manager, polybar, rofi, kitty, and anything else that uses config files to the same colorscheme. to set the theme, simply type

```
source systemtheme name-of-theme
```

For a persistent theme, include the above line in a startup file.

`systemtheme` has three hard-coded themes, `onedark`, `dracula`, and `gruvbox`. To add more, just put them in the themes.sh file after installing using the following syntax.

```
theme-name=("#hexvalue1" "#hexvalue2" ... )
```  

Each theme sets hex values for the following list of environment variables.

```
BACKGROUND  RED  GREEN  YELLOW  BLUE  MAGENTA  CYAN  FOREGROUND  WHITE 
BACKGROUND_ALT  RED_ALT  GREEN_ALT  YELLOW_ALT  BLUE_ALT  MAGENTA_ALT  CYAN_ALT  FOREGROUND_ALT
```

The 'ALT' colors are the darker variants.


# Installation
To install, navigate to the desired directory and type

```
git clone https://gitlab.com/samlee91/systemtheme.git
./install.sh
```

The user may change the default location of config files in the install script if desired.


# Theming Programs With Your System Theme
Each program works slightly differently and many programs natively suport environment variable substitution. `kitty` and `dunst` do not, so the `STKitty` and `STDunst` shell commands are included in this install to acomplish that.

Below is a brief explaination of the necessarry config file format for various programs. For full examples, see the `example_configs` directory.


## Polybar 
The polybar config file natively supports the use of environment variables. To use the colors of your system theme in polybar, use the following format. 
```
background = ${env:MAGENTA}
```

## Rofi
Rofi natively supports environment variables. In your config.rasi file, use your system colors as follows.
```
background-color: ${GREEN}
```

## Bspwm
The bspwmrc file is a shell script, so your system colors can be used as follows.
```
bspc config focused_border_color  $BLUE
```

## Kitty
The config file for kitty does not natively support environment variable substitution, but the STKitty command will do it for you. format your kitty config file as follows.
```
color3  ${YELLOW}
```

## Dunst
Dunst does not natively support environment variables, but the STDunst command will do it for you. format your dunstrc file as follows.
```
frame_color = "${CYAN}"
```


## Vim
To set highlight group parameters to environment variables in vim, use the following syntax in your `~/.vim/colors/name-of-theme` file.
```
exec "highlight name-of-group guifg="$GREEN
```

## Neovim
see [this repo](https://gitlab.com/samlee91/nvim-systemtheme) for a systemtheme plugin.


## Everything Else
If the config file that you are interested in does not support environment variable expansion, there is a good chance that you can make it happen with the `envsubst` command as used in the `ST...` commands.
