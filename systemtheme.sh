#! /usr/bin/env zsh

source /home/sam/.config/systemtheme/themes.sh


export SYSTEM_THEME="$1"
export TRANSPARENT="#00000000"

for (( i = 1; i <= 17; i++ )); do
   export $color_names[i]=${(P)1[$i]}
done


