#! /usr/bin/env zsh

kitty_config=/home/sam/.config/kitty/kitty.conf
[ -f $kitty_config ] && envsubst < $kitty_config | kitty -c /dev/stdin &

