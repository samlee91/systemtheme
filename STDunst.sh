#! /usr/bin/env zsh

dunst_config=/home/sam/.config/dunst/dunstrc
[ -f $dunst_config ] && envsubst < $dunst_config | dunst -c - &

